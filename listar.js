const htmlRow = document.getElementById("lista-pokemones");

const generarCards = async() => {
    const pokemones = await listarPokemones();

    for(let i = 0; i < pokemones.length; i++) {
        const pokemon = pokemones[i];
        
        const pokemonData = await getPokemon(pokemon.pokemon_species.url);
        console.log(pokemonData);
        const pokemonDataFinal = await getPokemon(pokemonData.varieties[0].pokemon.url);

        let card = document.createElement("div");
        card.className = "card";
        card.style = "width: 18rem;";
        htmlRow.appendChild(card);

        let imagen = document.createElement("img");
        imagen.src = pokemonDataFinal.sprites.other.dream_world.front_default;
        imagen.className = "card-img-top"
        imagen.alt = pokemon.pokemon_species.name;

        card.appendChild(imagen);

        let cardBody = document.createElement("div");
        cardBody.className = "card-body";

        let cardTitle = document.createElement("h5");
        cardTitle.className = "card-title";
        cardTitle.innerHTML = pokemon.pokemon_species.name;

        let cardText = document.createElement("p");
        cardText.className = "card-text";

        for (let j = 0; j < pokemonData.flavor_text_entries.length; j++) {
            const flavorElement = pokemonData.flavor_text_entries[j];
            if (flavorElement.language.name == "es"){
                cardText.innerHTML = flavorElement.flavor_text;
                break;
            }
        }

        cardBody.appendChild(cardTitle);
        cardBody.appendChild(cardText);

        card.appendChild(cardBody);
    }
}

{/* <div class="card" style="width: 18rem;">
        <img src="..." class="card-img-top" alt="...">
        <div class="card-body">
            <h5 class="card-title">Card title</h5>
            <p class="card-text">Some quick example text to build on the card title and make up the bulk of the
                card's
                content.</p>
            <a href="#" class="btn btn-primary">Go somewhere</a>
        </div>
</div> */}

generarCards();
// const tbody = document.querySelector('#tbl-productos tbody');
// let mostrar_datos = async() => {
//     let productos = await listarPokemones();
//     tbody.innerHTML = '';
//     for (let i = 0; i < productos.length; i++) {
//         let fila = tbody.insertRow();
//         fila.insertCell().innerHTML = productos[i]['codigo'];
//         fila.insertCell().innerHTML = productos[i]['nombre'];
//         fila.insertCell().innerHTML = productos[i]['precio'];
//         fila.insertCell().innerHTML = productos[i]['descripcion'];
//     }
// };
// mostrar_datos();