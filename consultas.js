let listarPokemones = async() => {
    const parametro = {
        method: 'get',
        url: 'https://pokeapi.co/api/v2/pokedex/2/',
        responseType: 'json'
    };
    
    let productos;
    await axios(parametro)
        .then((res) => {
            // console.log(res.data.pokemon_entries);
            productos = res.data.pokemon_entries;
        })
        .catch((err) => {
            console.log(err);
        });
    return productos;
};

let getPokemon = async(ruta) => {
    const parametro = {
        method: 'get',
        url: ruta,
        responseType: 'json'
    };
    
    let pokemon;
    await axios(parametro)
        .then((res) => {
            pokemon = res.data;
        })
        .catch((err) => {
            console.log(err);
        });
    return pokemon;
};

